# Objectif

## Objectif : Construire ses propres rôles pour déployer un wordpress sous docker
 :

    Création d'un serveur sous debian 11 avec vagrant
    Création des rôles ansible suivants :
        
        
- common : ce rôle doit regrouper toutes vos pratiques d'installation par défaut (création d'utilisateur, mise à jour système, paramétrage de la timezone, hostname...)
        
- docker : ce rôle doit vous permettre l'installation de docker et docker-compose
        
- dck-wordpress : ce rôle vous permet de déployer un wordpress sous docker (penser à utiliser le docker-compose du précédent TP)
    
    L'ensemble des rôles peuvent être appelé par le playbook principale main.yml
    Essayer d'utiliser le provisionner ansible local pour vous permettre d'exécuter les playbooks directement sur la machine guest


## 1 - Prérequis sur la machine invitée :

### Configurer le SSH :

Dans 

```
/etc/ssh/sshd_config
```
Décommenter et modifier la ligne :

```
PasswordAuthentication yes
```

Puis redémarrer le service SSH :

```
systemctl restart ssh
```

### Configurer SUDO

Installer et configurer sudo pour l'utilisateur :

```
apt install sudo
```

Puis dans /etc/sudoers ajouter la ligne :

> user      ALL=(ALL) ALL


## 2 - Configuration SSH

### Création d'une clé SSH sur la machine locale

```
sudo ssh-keygen
```
Puis copie de la clé publique sur le serveur Debian :
```
sudo ssh-copy-id -i ~/.ssh/id_rsa_key.pub user@machine
```

Configurer le fichier host qui se trouve à la racine du projet tp_ansible :

```
[all]

tpansible ansible_host=<192.168.X.X> ansible_user=<username> become=true
```

Et dans ~/.ssh/config (créer le fichier config) :

```
host tpansible
	hostname <192.168.X.X>
	user <username>
	identityfile ~/.ssh/<id_rsa_ansible>
```


## 3 - Lancement du playbook

Ensuite on peut lancer le playbook avec la commande -K et renseigner le mot de passe de l'utilisateur :

```
ansible-playbook main.yml -K
```
