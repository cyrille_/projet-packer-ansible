packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "ami_prefix" {
  type    = string
  default = "groupe3-ci"
}

locals {
  timestamp = regex_replace(timestamp(), "[: TZ]", "")
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "${var.ami_prefix}-${local.timestamp}"
  instance_type = "t3.micro"
  region        = "eu-north-1"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-xenial-16.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}


build {
  name = "learn-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]


  provisioner "shell" {
    inline = [
      "echo Installing ansible",
      "sleep 30",
      "sudo apt update",
      "sudo apt upgrade -y",
      "sudo apt-get install software-properties-common -y",
      "sudo add-apt-repository --yes --update ppa:ansible/ansible",
      "sudo apt install ansible -y"
    ]
  }

  provisioner "ansible-local" {
    playbook_file = "./provision_ansible/main.yml"
    role_paths = [
      "./provision_ansible/roles/common",
      "./provision_ansible/roles/docker", 
      "./provision_ansible/roles/flask",
      "./provision_ansible/roles/traefik-authelia"
    ]
    group_vars = "./provision_ansible/group_vars/"

  }
}
